.. Gender Classification documentation master file, created by
   sphinx-quickstart on Tue Jul 30 12:26:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gender Classification's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Documentation Model File
=====================
.. autoclass:: model.model.ModelName
    :members:

    .. automethod:: __init__

Documentation Predict File
=====================
.. autoclass:: predict.Predict
    :members:

    .. automethod:: __init__


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
