import pandas as pd
import numpy as np
import pickle


from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers.embeddings import Embedding
from keras.layers import Dense,LSTM, Dropout, Activation
from keras.layers import BatchNormalization







class ModelName:
	"""
		**This i a conceptual class created for the representation of training engine for gender classification**

		:param epochs: The number of iteration/epoch desired to train the model
		:type epochs: int
		:param validation_split: The fractional amount representing validation data size with respect to whole data size. This value should lie between 0 to 1
		:type validation_split: float
		:param male_name_path: the path directory of the male name dataset to train the model
		:type male_name_path: str
		:param female_name_path: the path directory of the female name dataset to train the model
		:type female_name_path: str
	"""
	def __init__(self,epochs = 10, validation_split = 0.4, male_name_path = "/home/shushant/Desktop/name_gender_classification/profanity/profanity-detection/data/male.csv",female_name_path = "/home/shushant/Desktop/name_gender_classification/profanity/profanity-detection/data/female.csv"):
		"""
			**Constructor method**
		"""
		self.male_name_df = pd.read_csv(male_name_path,sep="\t")
		self.female_name_df= pd.read_csv(female_name_path,sep="\t")
		self.epochs = epochs
		self.validation_split = validation_split


		

	def train_model(self):
		"""
			**This method is used to prepare data, build model and train the model for gender classification**

			- Code Snippet::

				final_df = self.male_name_df.append(self.female_name_df, ignore_index=True)
				final_df=final_df.sample(frac=1).reset_index(drop=True)
				final_df.loc[:,"Name"] = final_df.Name.apply(lambda x : str(x))
				final_df["Label"] = final_df["Label"].replace(["male" , "female"] , [1 , 0])
				labels = final_df["Label"]

				#Building tokenizer
				tokenizer = Tokenizer(char_level=True, oov_token='UNK',num_words = 20000)
				tokenizer.fit_on_texts(final_df["Name"])
				sequences = tokenizer.texts_to_sequences(final_df['Name'])
				data = pad_sequences(sequences,maxlen = 10)

				#Build model
				model = Sequential()
				model.add(Embedding(20000, 50, input_length=10))
				model.add(LSTM(50, dropout=0.2, recurrent_dropout=0.2))
				model.add(BatchNormalization())
				model.add(Dropout(0.4))
				model.add(Dense(50, activation="tanh"))
				model.add(BatchNormalization())
				model.add(Dropout(0.4))
				model.add(Dense(50, activation="tanh"))
				model.add(BatchNormalization())
				model.add(Dropout(0.4))
				model.add(Dense(1, activation="sigmoid"))
				model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
				model.fit(data, np.array(labels),validation_split= self.validation_split,epochs = self.epochs)
				
				
				# saving trained model
				with open('../serializer/male_female_identifier.pickle', 'wb') as handle:
					pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)

				# saving tokenizer
				with open('../serializer/male_female_tokenizer.pickle', 'wb') as handle:
					pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
		"""
		final_df = self.male_name_df.append(self.female_name_df, ignore_index=True)
		final_df=final_df.sample(frac=1).reset_index(drop=True)
		final_df.loc[:,"Name"] = final_df.Name.apply(lambda x : str(x))
		final_df["Label"] = final_df["Label"].replace(["male" , "female"] , [1 , 0])
		labels = final_df["Label"]

		#Building tokenizer
		tokenizer = Tokenizer(char_level=True, oov_token='UNK',num_words = 20000)
		tokenizer.fit_on_texts(final_df["Name"])
		sequences = tokenizer.texts_to_sequences(final_df['Name'])
		data = pad_sequences(sequences,maxlen = 10)

		#Build model
		model = Sequential()
		model.add(Embedding(20000, 50, input_length=10))
		model.add(LSTM(50, dropout=0.2, recurrent_dropout=0.2))
		model.add(BatchNormalization())
		model.add(Dropout(0.4))
		model.add(Dense(50, activation="tanh"))
		model.add(BatchNormalization())
		model.add(Dropout(0.4))
		model.add(Dense(50, activation="tanh"))
		model.add(BatchNormalization())
		model.add(Dropout(0.4))
		model.add(Dense(1, activation="sigmoid"))
		model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
		model.fit(data, np.array(labels),validation_split= self.validation_split,epochs = self.epochs)
		
		
		# saving trained model
		with open('../serializer/male_female_identifier.pickle', 'wb') as handle:
			pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)

		# saving tokenizer
		with open('../serializer/male_female_tokenizer.pickle', 'wb') as handle:
			pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
