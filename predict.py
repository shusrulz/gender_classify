import pickle
from keras.preprocessing.sequence import pad_sequences
import numpy as np

class Predict:
    """
        **This class is used to predict the type of gender from the trained gender classification model**
    """
    def __init__(self):
        """
            **Constructor method**
        """
        with open('./serializer/male_female_tokenizer.pickle', 'rb') as handle:
            self.tokenizer = pickle.load(handle)
        with open('./serializer/male_female_identifier.pickle', 'rb') as handle:
            self.loaded_model = pickle.load(handle)

    def predict_single(self,name):
        """
            **This method is used to predict the gender type of single token name**

            :param name: Testing name to classify gender
            :type name: str
            :return: gender of the test name
            :rtype: str

            - Code Snippet::
            
                text = np.array([name])
                sequences_test = self.tokenizer.texts_to_sequences(text)
                sequence = pad_sequences(sequences_test,maxlen = 10)
                prediction = self.loaded_model.predict(sequence)
                if (prediction>0.45):
                    return("male")
                else:
                    return("female")
        """
        text = np.array([name])
        sequences_test = self.tokenizer.texts_to_sequences(text)
        sequence = pad_sequences(sequences_test,maxlen = 10)
        prediction = self.loaded_model.predict(sequence)
        if (prediction>0.45):
            return("male")
        else:
            return("female")

# obj2 = Predict()
# text_input = str(input("Enter the name to be classified :: "))
# output_gender = obj2.predict_single(text_input)
# print(output_gender)